#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "estudiantes.h"


float desvStd(estudiante curso[], int cuantos_estudiantes){
int i=0;
float largo = 0.0, suma = 0.0, promZ = 0.0, suma1 = 0.0;

 for (i = 0; i<cuantos_estudiantes;i++)//calculo del promedio
 {
 	promZ = promZ + curso[i].prom;//se suman todos los .prom
 }
 promZ = promZ/cuantos_estudiantes;//para despues dividirlos por el numero de estudiantes
 for (i = 0; i<cuantos_estudiantes; i++)
 {
 	suma = (promZ-curso[i].prom)*(promZ-curso[i].prom);//se eleva al cuadrado para sacar la devstd
 	suma1 = suma1 + suma;// mientras se suma, hasta dejarlo en una sola variable

 }
 suma1 = sqrt(suma1/ (cuantos_estudiantes-1));//se saca la raiz cuadrada de la suma creada y dividida por el n° de estudiantes menos 1
 return suma1;
}

float menor(estudiante curso[], int cuantos_estudiantes){
	int i,id=0;//Declaración de variables
float guardar= 7.0;// se declara con un 7 para compararlo con la nota más alta
	
	
	for (i=0;i<cuantos_estudiantes;i++)
	{
	if (guardar > curso[i].prom)
		{
			guardar=curso[i].prom;
			id = i;// se cuanda una id, para así saber de quien es la nota
		}
	}
	printf("\n   %s %s %s   ",curso[id].nombre , curso[id].apellidoP, curso[id].apellidoM);
	printf("\n   %.2f\n",guardar );// se imprime la nota y de quien es
	return id;//retorna id, por retornar algo asdasasdas
}

float mayor(estudiante curso[], int cuantos_estudiantes){
int i,id=0;
float guardar;
	
	guardar = 0;
	for (i=0;i<cuantos_estudiantes;i++)
	{
	if (guardar < curso[i].prom)// se busca quien es mas grande,
		{
			guardar=curso[i].prom;// guardando el más grande, y despues volverlo a comparar
			id = i;// el mismo metodo, se guarda la "id" para saber de quien es el promedio
		}
	}
	printf("\n   %s %s %s  ",curso[id].nombre , curso[id].apellidoP, curso[id].apellidoM);
	printf("\n   %.2f\n",guardar );// se imprime el prom y de quien es 
	return id;
}

void registroCurso(estudiante curso[], int cuantos_estudiantes){
	//debe registrar las calificaciones 
	int i = 0;
	float prom = 0;
	printf("------------------\n");
	printf("INGRESO DE NOTAS\n");
	printf("------------------\n");
	for(i = 0; i <cuantos_estudiantes; i++)//pedir ingreso de todos los estudiantes
	{
//restricciones
		printf("Para el estudiante: \n");
		printf("%s %s %s\n",curso[i].nombre , curso[i].apellidoP, curso[i].apellidoM);
		printf("Proyecto 1: \n");
		scanf("%f",&curso[i].asig_1.proy1);
		printf("Proyecto 2: \n");	
		scanf("%f",&curso[i].asig_1.proy2);
		printf("Proyecto 3\n");
		scanf("%f",&curso[i].asig_1.proy3);
		printf("Control 1\n");
		scanf("%f",&curso[i].asig_1.cont1);
		printf("Control 2\n");
		scanf("%f",&curso[i].asig_1.cont2);
		printf("Control 3\n");
		scanf("%f",&curso[i].asig_1.cont3);
		printf("Control 4\n");
		scanf("%f",&curso[i].asig_1.cont4);
		printf("Control 5\n");
		scanf("%f",&curso[i].asig_1.cont5);
		printf("Control 6\n");
		scanf("%f",&curso[i].asig_1.cont6);
		prom = curso[i].asig_1.proy1* 0.2 + curso[i].asig_1.proy2* 0.2 + curso[i].asig_1.proy3* 0.3 + curso[i].asig_1.cont1*0.05 + curso[i].asig_1.cont2*0.05 + curso[i].asig_1.cont3*0.05 + curso[i].asig_1.cont4*0.05 + curso[i].asig_1.cont5*0.05 + curso[i].asig_1.cont6*0.05;
		curso[i].prom = prom;//se saca el prom, para despues imrpimirlo y guardarlo
		printf("El promedio del estudiante es : %.2f\n",curso[i].prom  );
	}
}

void clasificarEstudiantes(char path[], estudiante curso[], int cuantos_estudiantes){
	int i = 0,cont_a = 0, cont_r = 0;
	float guardar = 0.0;
	FILE *aprobados, *reprobados;
	aprobados = fopen("Aprobados.txt", "w");//cración de archivos
	reprobados = fopen("reprobados.txt","w");
	for (i = 0; i<cuantos_estudiantes; i++)
	{
		if (curso[i].prom >= 4.0)// si cumple, lo imprime en aprobados
		{
			fprintf(aprobados, "%s %s %s : %.2f\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM, curso[i].prom );
		}
		else//si no, en resprobados 
		{
			fprintf(reprobados, "%s %s %s : %.2f\n",curso[i].nombre,curso[i].apellidoP,curso[i].apellidoM, curso[i].prom );
		}
	}
	printf("\n   *Exelente!!, los estudiantes se clasificaron con exito \n");
	fclose(aprobados);
	fclose(reprobados);

}	

void metricasEstudiantes(estudiante curso[], int cuantos_estudiantes)
{// la media, el mayor , el menor y el promedio 
	printf("   La desviación estandar de los promedios es: %.2f\n", desvStd(curso,cuantos_estudiantes));
	printf("   ----------------------------\n");
	printf("   El promedio mayor es de : \n");
	mayor(curso,cuantos_estudiantes);
	printf("   ----------------------------\n");
	printf("   EL promedio menor es de : \n");
	menor(curso,cuantos_estudiantes);
	printf("   ----------------------------\n");

}


void menu(estudiante curso[], int cuantos_estudiantes){
	int opcion;
    do{
		printf( "\n   1. Cargar Estudiantes" );
		printf( "\n   2. Ingresar notas" );
		printf( "\n   3. Mostrar Promedios" );
		printf( "\n   4. Almacenar en archivo" );
		printf( "\n   5. Clasificar Estudiantes " );
		printf( "\n   6. Salir." );
		printf( "\n\n   Introduzca opción (1-6): " );
		scanf( "%d", &opcion );
        switch ( opcion ){
			case 1: cargarEstudiantes("estudiantes.txt", curso); //carga en lote una lista en un arreglo de registros
					break;

			case 2:  registroCurso(curso, cuantos_estudiantes);// Realiza ingreso de notas en el registro curso 
					break;

			case 3: metricasEstudiantes(curso, cuantos_estudiantes); //presenta métricas de disperción, tales como mejor promedio; más bajo; desviación estándard.
					break;

			case 4: grabarEstudiantes("test.txt",curso);
					break;
            case 5: clasificarEstudiantes("destino", curso, cuantos_estudiantes); // clasi
            		break;
         }

    } while ( opcion != 6 );
}

int main(){
int cuantos_estudiantes=0;
	estudiante curso[30]; 
	printf("Cuantos estudiantes va a ingresar?/ej: 24\n");
	scanf("%d",&cuantos_estudiantes);
	menu(curso, cuantos_estudiantes); 

	return EXIT_SUCCESS; 
}